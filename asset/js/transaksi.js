if (sessionStorage.getItem("email") == null) {
	Swal.fire("MAAF!", "Mohon Login dulu!", "error");
	setTimeout(function () {
		window.location.href = rest.http + "auth/login";
	}, 2000);
}
$("#nis").on("change", function () {
	var ids = $("#nis").val();
	var form = new FormData();
	var settings = {
		url: rest.uri + "transaksi/getnis?siswa_nis=" + $("#nis").val(),
		method: "GET",
		timeout: 0,
		headers: {
			"X-key": rest.token,
			Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
		},
		processData: false,
		mimeType: "multipart/form-data",
		contentType: false,
		data: {
			siswa_nis: ids,
		},
	};

	$.ajax(settings).done(function (response) {
		var sis = JSON.parse(response);
		//console.log(sis.data[0].id_siswa);
		$("#nama_siswa").val(sis.data[0].siswa_nama);
		$("#kelas").val(sis.data[0].siswa_kelas);
		$("#jurusan").val(sis.data[0].siswa_jurusan);
		$("#id_siswa").val(sis.data[0].id_siswa);
	});
});

$("#pinjam").click(function (e) {
	e.preventDefault();
	var form = new FormData();
	form.append("id_siswa", $("#id_siswa").val());
	form.append("tgl_pinjam", $("#tgl_pinjam").val());
	form.append("tgl_kembali", $("#tgl_kembali").val());
	form.append("nama_buku", $("#nama_buku").val());

	var settings = {
		url: rest.uri + "transaksi/tran",
		method: "POST",
		timeout: 0,
		headers: {
			"X-key": rest.token,
			Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
		},
		processData: false,
		mimeType: "multipart/form-data",
		contentType: false,
		data: form,
		error: function (jqXHR, textStatus, errorThrown) {
			let pesan = JSON.parse(jqXHR.responseText);

			const Toast = Swal.mixin({
				toast: true,
				position: "top-end",
				showConfirmButton: false,
				timer: 1500,
				timerProgressBar: true,
				onOpen: (toast) => {
					toast.addEventListener("mouseenter", Swal.stopTimer);
					toast.addEventListener("mouseleave", Swal.resumeTimer);
				},
			});

			Toast.fire({
				icon: "error",
				title: [pesan.message],
			});
		},
	};

	$.ajax(settings).done(function (response) {
		swal.fire({
			icon: "success",
			title: "transaksi Berhasil di Upload",
		});
		getdatatran();
	});
});

getdatatran();

function getdatatran() {
	$("#datatran").html("");
	//e.preventDefault();
	var form = new FormData();

	var settings = {
		url: rest.uri + "transaksi/pinjam",
		method: "GET",
		timeout: 0,
		headers: {
			"X-key":
				"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxMzU2OTk5NTI0LCJuYmYiOjEzNTcwMDAwMDAsImlkX3VzZXIiOiIxIiwidXNlcm5hbWUiOiJzaWdpdGIiLCJlbWFpbCI6InNpZ2l0YnVkaUBnbWFpbC5jb20ifQ.8LjBdRkcmQ6T_Elw74tRqRUhRDezNEa7kuO14SPSZRs",

			Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
		},
		processData: false,
		mimeType: "multipart/form-data",
		contentType: false,
		data: form,
	};
	$.ajax(settings).done(function (response) {
		let data = JSON.parse(response);
		//$(".remove").remove();
		var siswa = "";
		$.each(data.data, function (key, value) {
			siswa +=
				"<tr class='remove'>" +
				"<td>" +
				value.siswa_nis +
				"</td>" +
				"<td>" +
				value.siswa_nama +
				"</td>" +
				"<td>" +
				value.buku +
				"</td>" +
				"<td>" +
				value.tgl_pinjam +
				"</td>" +
				"<td>" +
				value.tgl_kembali +
				"</td>" +
				"<td>" +
				value.status +
				"</td>" +
				"<td> <button type='button' class='btn btn-danger ' onclick='dikembalikan(" +
				value.id_transaksi +
				")' id='hapustran' data-id='" +
				value.id_transaksi +
				"' data-toggle='modal' >" +
				"hapus" +
				"<button type='button' class='btn btn-primary ' id='kembalikan' data-id='" +
				value.id_transaksi +
				"'onclick='dikembalikan(" +
				value.id_transaksi +
				")'>" +
				"kembalikan " +
				"</button>" +
				"</td>" +
				"</tr>";
		});
		$("#datatran").append(siswa);
	});
}

function dikembalikan(id) {
	var settings = {
		url: rest.uri + "transaksi/kembalikan",
		method: "PUT",
		timeout: 0,
		headers: {
			"X-key":
				"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxMzU2OTk5NTI0LCJuYmYiOjEzNTcwMDAwMDAsImlkX3VzZXIiOiIxIiwidXNlcm5hbWUiOiJzaWdpdGIiLCJlbWFpbCI6InNpZ2l0YnVkaUBnbWFpbC5jb20ifQ.8LjBdRkcmQ6T_Elw74tRqRUhRDezNEa7kuO14SPSZRs",
			Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
			"Content-Type": "application/x-www-form-urlencoded",
		},
		data: {
			id_transaksi: id,
		},
	};

	$.ajax(settings).done(function (response) {
		swal.fire({
			icon: "success",
			title: "Data Berhasil di dikembalikan",
		});
		getdatatran();
	});
}
function dihapus(id) {
	var settings = {
		url: "http://localhost/mpj_cellenge/transaksi/deletetran",
		method: "DELETE",
		timeout: 0,
		headers: {
			"X-key":
				"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxMzU2OTk5NTI0LCJuYmYiOjEzNTcwMDAwMDAsImlkX3VzZXIiOiIxIiwidXNlcm5hbWUiOiJzaWdpdGIiLCJlbWFpbCI6InNpZ2l0YnVkaUBnbWFpbC5jb20ifQ.8LjBdRkcmQ6T_Elw74tRqRUhRDezNEa7kuO14SPSZRs",
			Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
			"Content-Type": "application/x-www-form-urlencoded",
		},
		data: {
			id_transaksi: id,
		},
	};

	$.ajax(settings).done(function (response) {
		swal.fire({
			icon: "success",
			title: "Data Berhasil di dikembalikan",
		});
		getdatatran();
	});
}

getkembali();

function getkembali() {
	$("#datakem").html("");
	//e.preventDefault();
	var form = new FormData();

	var settings = {
		url: rest.uri + "transaksi/kembali",
		method: "GET",
		timeout: 0,
		headers: {
			"X-key":
				"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxMzU2OTk5NTI0LCJuYmYiOjEzNTcwMDAwMDAsImlkX3VzZXIiOiIxIiwidXNlcm5hbWUiOiJzaWdpdGIiLCJlbWFpbCI6InNpZ2l0YnVkaUBnbWFpbC5jb20ifQ.8LjBdRkcmQ6T_Elw74tRqRUhRDezNEa7kuO14SPSZRs",

			Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
		},
		processData: false,
		mimeType: "multipart/form-data",
		contentType: false,
		data: form,
	};
	$.ajax(settings).done(function (response) {
		let data = JSON.parse(response);
		//$(".remove").remove();
		var siswa = "";
		$.each(data.data, function (key, value) {
			siswa +=
				"<tr class='remove'>" +
				"<td>" +
				value.siswa_nis +
				"</td>" +
				"<td>" +
				value.siswa_nama +
				"</td>" +
				"<td>" +
				value.buku +
				"</td>" +
				"<td>" +
				value.tgl_pinjam +
				"</td>" +
				"<td>" +
				value.tgl_kembali +
				"</td>" +
				"<td>" +
				value.status +
				"</td>" +
				"</tr>";
		});
		$("#datakem").append(siswa);
	});
}
