$(document).ready(function () {
	getdata();

	function getdata() {
		if (sessionStorage.getItem("email") == null) {
			Swal.fire("MAAF!", "Mohon Login dulu!", "error");
			setTimeout(function () {
				window.location.href = rest.http + "auth/login";
			}, 2000);
		}

		var form = new FormData();

		var settings = {
			url: rest.uri + "buku",
			method: "GET",
			timeout: 0,
			headers: {
				"X-key": rest.token,
				Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
			},
			processData: false,
			mimeType: "multipart/form-data",
			contentType: false,
			data: form,
		};
		$.ajax(settings).done(function (response) {
			let data = JSON.parse(response);
			$(".remove").remove();

			$.each(data.data, function (key, value) {
				var siswa =
					"<tr class='remove'>" +
					"<td>" +
					value.nama_buku +
					"</td>" +
					"<td>  <img height='100' src='" +
					rest.uri +
					"asset/img/" +
					value.gambar +
					"'></td>" +
					"<td> <button type='button' class='btn btn-danger ' id='hapusbuku' data-id='" +
					value.id_buku +
					"' data-toggle='modal' >" +
					"Hapus" +
					"<button type='button' class='btn btn-primary ' id='editbuku' data-id='" +
					value.id_buku +
					"' data-toggle='modal' data-target='#modelEdit'>" +
					"Edit " +
					"</button>" +
					"</td>" +
					"</tr>";

				$("#data-buku").append(siswa);
			});
		});
	}

	$("#myformbuku").validate({
		rules: {
			nama_buku: {
				required: true,
				minlength: 5,
			},
			gambar: {
				required: true,
			},
		},
		submitHandler: function (myformbuku) {
			var file = $("#gambar")[0].files[0];
			Swal.fire({
				title: "Are you sure?",
				text: "You won't be able to revert this!",
				icon: "success",
				showCancelButton: true,
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				confirmButtonText: "Yes, Upload Produk it!",
			}).then((result) => {
				if (result.value) {
					var form = new FormData();
					form.append("nama_buku", $("#nama_buku").val());
					form.append("gambar", file);

					var settings = {
						url: rest.uri + "buku",
						method: "POST",
						timeout: 0,
						headers: {
							"X-key": rest.token,
							Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
						},
						processData: false,
						mimeType: "multipart/form-data",
						contentType: false,
						data: form,
					};

					$.ajax(settings).done(function (response) {
						const Toast = Swal.mixin({
							toast: true,
							position: "top-end",
							showConfirmButton: false,
							timer: 1500,
							timerProgressBar: true,
							onOpen: (toast) => {
								toast.addEventListener("mouseenter", Swal.stopTimer);
								toast.addEventListener("mouseleave", Swal.resumeTimer);
							},
						});

						Toast.fire({
							icon: "success",
							title: "Data Berhasil di Upload",
						});
						$("#myformbuku")[0].reset();
						$("#modelId").modal("hide");
						//  $(this).parent().remove();
						getdata();
					});
				}
			});
		},
	});

	$(document).on("click", "#hapusbuku", function (e) {
		e.preventDefault();
		Swal.fire({
			title: "Apakah ingin Mengahapus buku ini?",
			text: "h!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!",
		}).then((result) => {
			if (result.value) {
				var settings = {
					url: rest.uri + "buku",
					method: "DELETE",
					timeout: 0,
					headers: {
						"X-key": rest.token,
						Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
						"Content-Type": "application/x-www-form-urlencoded",
					},
					data: {
						id_buku: $(this).attr("data-id"),
					},
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: "top-center",
						icon: "success",
						title: "Your work has been saved",
						showConfirmButton: false,
						timer: 1500,
					});

					getdata();
				});
			}
		});
	});

	$(document).on("click", "#editbuku", function () {
		//e.preventDefault();
		var form = new FormData();
		form.append("id_buku", $(this).attr("data-id"));
		var settings = {
			url: rest.uri + "buku/selectBy?id_buku=" + $(this).attr("data-id"),
			method: "GET",
			timeout: 0,
			headers: {
				"X-key": rest.token,
				Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
			},
			processData: false,
			mimeType: "multipart/form-data",
			contentType: false,
			data: form,
		};

		$.ajax(settings).done(function (response) {
			let jsn = JSON.parse(response);
			//console.log(jsn);

			$.each(jsn.data, function (key, value) {
				$("#u_nama_buku").val(value.nama_buku);
				$("#id_buku").val(value.id_buku);
			});
		});
	});

	$("#updateformbuku").validate({
		rules: {
			nama_buku: {
				required: true,
				minlength: 5,
			},
		},
		submitHandler: function (updateformbuku) {
			var file = $("#ub_gambar")[0].files[0];
			Swal.fire({
				title: "Are you sure?",
				text: "You won't be able to revert this!",
				icon: "success",
				showCancelButton: true,
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				confirmButtonText: "Yes, Upload Produk it!",
			}).then((result) => {
				if (result.value) {
					var form = new FormData();
					//form.append("id_siswa", $('#id_siswa').val());
					form.append("nama_buku", $("#u_nama_buku").val());
					form.append("gambar", file);
					form.append("_method", "PUT");
					form.append("id_buku", $("#id_buku").val());

					var settings = {
						url: rest.uri + "buku",
						method: "POST",
						timeout: 0,
						headers: {
							"X-key": rest.token,
							Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
						},
						processData: false,
						mimeType: "multipart/form-data",
						contentType: false,
						data: form,
					};

					$.ajax(settings).done(function (response) {
						const Toast = Swal.mixin({
							toast: true,
							position: "top-end",
							showConfirmButton: false,
							timer: 1500,
							timerProgressBar: true,
							onOpen: (toast) => {
								toast.addEventListener("mouseenter", Swal.stopTimer);
								toast.addEventListener("mouseleave", Swal.resumeTimer);
							},
						});

						Toast.fire({
							icon: "success",
							title: "Data Berhasil di Upload",
						});
						$("#updateformbuku")[0].reset();
						$("#modelEdit").modal("hide");
						//  $(this).parent().remove();
						getdata();
					});
				}
			});
		},
	});
});
