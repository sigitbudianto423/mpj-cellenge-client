<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template {

    //protected $_ci;
    
    function __construct(){
    //   parent::__construct();
       
    }
    function templateAuth( $content, $data = null){
        $ci = get_instance();
        $data['content'] = $ci->load->view($content , $data, TRUE);

        $ci->load->view('templates/auth/header');
        //$ci->load->view('templates/auth/navigation');
        $ci->load->view('templates/auth/content', $data);
        $ci->load->view('templates/auth/footer');
        
    }

    function templateAdmin( $content, $data = null){
        $ci = get_instance();
        $data['content'] = $ci->load->view($content , $data, TRUE);

        $ci->load->view('templates/admin/header');
        //$ci->load->view('templates/admin/sidebar');
        $ci->load->view('templates/admin/navbar');
        $ci->load->view('templates/admin/content', $data);
        $ci->load->view('templates/admin/footer');
        
    }


}
