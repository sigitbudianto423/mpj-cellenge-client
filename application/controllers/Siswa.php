<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller 
{

    function __construct(){
        parent::__construct();
		$this->load->library('template');
	
    }


    function index(){
        $this->template->templateAdmin('admin/siswa');
    }

}