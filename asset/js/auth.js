$(document).ready(function () {
	if (sessionStorage.getItem("email")) {
		window.location.href = rest.http + "transaksi";
	}

	$("#login").click(function (e) {
		e.preventDefault();

		var form = new FormData();
		form.append("email", $("#email").val());
		form.append("password", $("#password").val());

		var settings = {
			url: rest.uri + "auth/login",
			method: "POST",
			timeout: 0,
			headers: {
				"X-key": rest.token,
				Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
			},
			processData: false,
			mimeType: "multipart/form-data",
			contentType: false,
			data: form,
			error: function (jqXHR, textStatus, errorThrown) {
				let pesan = JSON.parse(jqXHR.responseText);

				const Toast = Swal.mixin({
					toast: true,
					position: "top-end",
					showConfirmButton: false,
					timer: 1500,
					timerProgressBar: true,
					onOpen: (toast) => {
						toast.addEventListener("mouseenter", Swal.stopTimer);
						toast.addEventListener("mouseleave", Swal.resumeTimer);
					},
				});

				Toast.fire({
					icon: "error",
					title: [pesan.message],
				});
			},
		};

		$.ajax(settings).done(function (response) {
			let login = JSON.parse(response);
			let val = login.data;
			sessionStorage.setItem("username", val.username);
			sessionStorage.setItem("email", val.email);
			sessionStorage.setItem("password", val.password);

			if (val.level == "admin") {
				Swal.fire({
					position: "top-center",
					icon: "success",
					title: "Terimah kasih",
					showConfirmButton: false,
					timer: 1500,
				});
				setTimeout(function () {
					window.location.href = rest.http + "siswa";
				}, 2500);
			} else {
				Swal.fire({
					position: "top-center",
					icon: "success",
					title: "Terimah kasih",
					showConfirmButton: false,
					timer: 1500,
				});

				setTimeout(function () {
					window.location.href = rest.http + "user";
				}, 2500);
			}
		});
	});
});
