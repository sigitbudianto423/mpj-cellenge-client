<!-- Button trigger modal -->
<div class="container">
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modelId">
    Tambah Data Buku
    </button>
</div>

<!-- Modal -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
            <section id="form_tambah_data" class="col-md-6-sm-6 offsite-md-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" id="myformbuku">                    
                                    <div class="form-group">
                                    <label for="nama">Nama Buku</label>
                                    <input type="text"
                                        class="form-control" name="nama_buku" id="nama_buku" aria-describedby="helpId" placeholder="Nama Buku">
                                                        
                                    <div class="form-group">
                                    <label for="gambar">gambar</label>
                                    <input type="file"
                                        class="form-control" name="gambar" id="gambar" aria-describedby="helpId" >
                                    
                                <br>
                                <center>
                                <button class="btn  btn-primary" type="submit" id="submit">Tambah Data</button>
                                </center>
                        </form>
                    </div>
                </div>
            </section>  

            </div>
        
        </div>
    </div>
</div>



<div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"></h1>
         
          <div class="row">
            <div class="col-lg">

<table id="table" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead>
        <tr>
            
            <th>Nama Siswa</th>
            <th>Gambar</th>
            <th>Opsi</th>
            
        </tr>
    </thead>
    <tbody id="data-buku">
    </tbody>
</table>

 </div>
    
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


      <div class="modal fade" id="modelEdit" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data Siswa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
            <section id="form_edit_data" class="col-md-6-sm-6 offsite-md-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" id="updateformbuku">                    
                                    <div class="form-group">
                                    <label for="nama">Nama Buku</label>
                                    <input type="text"
                                        class="form-control" name="u_nama_buku" id="u_nama_buku" aria-describedby="helpId" placeholder="Nama Buku">
                                        <input type="hidden"
                                        class="form-control"  name="id_buku" id="id_buku" aria-describedby="helpId" placeholder="id_buku">
                                    <div class="form-group">
                                    <label for="gambar">gambar</label>
                                    <input type="file"
                                        class="form-control" name="ub_gambar" id="ub_gambar" aria-describedby="helpId" >
                                    
                                <br>
                                <center>
                                <button class="btn  btn-primary" type="submit" id="submitbuku">Edit Data Siswa</button>
                                </center>
                        </form>
                    </div>
                </div>
            </section>  

            </div>
        
        </div>
    </div>
</div>

<script src="<?= base_url('asset/');?>js/buku.js"></script>