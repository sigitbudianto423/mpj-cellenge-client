$(document).ready(function () {
	getdata();
	function getdata() {
		if (sessionStorage.getItem("email") == null) {
			Swal.fire("MAAF!", "Mohon Login dulu!", "error");
			setTimeout(function () {
				window.location.href = rest.http + "auth/login";
			}, 2000);
		}

		var form = new FormData();

		var settings = {
			url: rest.uri + "siswa",
			method: "GET",
			timeout: 0,
			headers: {
				"X-key": rest.token,
				Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
			},
			processData: false,
			mimeType: "multipart/form-data",
			contentType: false,
			data: form,
		};

		$.ajax(settings).done(function (response) {
			let data = JSON.parse(response);
			$(".remove").remove();

			$.each(data.data, function (key, value) {
				var siswa =
					"<tr class='remove'>" +
					"<td>" +
					value.siswa_nis +
					"</td>" +
					"<td>" +
					value.siswa_nama +
					"</td>" +
					"<td>" +
					value.siswa_kelas +
					"</td>" +
					"<td>" +
					value.siswa_jurusan +
					"</td>" +
					"<td>" +
					value.email +
					"</td>" +
					"<td>  <img height='100' src='" +
					rest.uri +
					"asset/img/" +
					value.gambar +
					"'></td>" +
					"<td> <button type='button' class='btn btn-danger ' id='btn-hapus' data-id='" +
					value.id_siswa +
					"' data-toggle='modal' >" +
					"Hapus" +
					"<button type='button' class='btn btn-primary ' id='btn-edit' data-id='" +
					value.id_siswa +
					"' data-toggle='modal' data-target='#modelEdit'>" +
					"Edit Data Siswa" +
					"</button>" +
					"</td>" +
					"</tr>";

				$("#data").append(siswa);
			});
		});
	}

	$("#myform").validate({
		rules: {
			siswa_nis: {
				required: true,
				minlength: 8,
				number: true,
			},
			siswa_nama: {
				required: true,
			},
			siswa_kelas: {
				required: true,
				number: true,
			},
			siswa_jurusan: {
				required: true,
			},
			email: {
				required: true,
			},
			gambar: {
				required: true,
			},
		},
		submitHandler: function (myform) {
			var file = $("#gambar")[0].files[0];
			Swal.fire({
				title: "Are you sure?",
				text: "You won't be able to revert this!",
				icon: "success",
				showCancelButton: true,
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				confirmButtonText: "Yes, Upload Produk it!",
			}).then((result) => {
				if (result.value) {
					var form = new FormData();
					form.append("siswa_nama", $("#siswa_nama").val());
					form.append("siswa_kelas", $("#siswa_kelas").val());
					form.append("siswa_jurusan", $("#siswa_jurusan").val());
					form.append("gambar", file);
					form.append("email", $("#email").val());
					form.append("siswa_nis", $("#siswa_nis").val());

					var settings = {
						url: rest.uri + "siswa",
						method: "POST",
						timeout: 0,
						headers: {
							"X-key": rest.token,
							Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
						},
						processData: false,
						mimeType: "multipart/form-data",
						contentType: false,
						data: form,
					};

					$.ajax(settings).done(function (response) {
						const Toast = Swal.mixin({
							toast: true,
							position: "top-end",
							showConfirmButton: false,
							timer: 1500,
							timerProgressBar: true,
							onOpen: (toast) => {
								toast.addEventListener("mouseenter", Swal.stopTimer);
								toast.addEventListener("mouseleave", Swal.resumeTimer);
							},
						});

						Toast.fire({
							icon: "success",
							title: "Data Berhasil di Upload",
						});
						$("#myform")[0].reset();
						$("#modelId").modal("hide");
						//  $(this).parent().remove();
						getdata();
					});
				}
			});
		},
	});

	$(document).on("click", "#btn-hapus", function (e) {
		e.preventDefault();
		Swal.fire({
			title: "Apakah ingin Mengahapus siswa ini?",
			text: "h!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!",
		}).then((result) => {
			if (result.value) {
				var settings = {
					url: rest.uri + "siswa",
					method: "DELETE",
					timeout: 0,
					headers: {
						"X-key": rest.token,
						Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
					},
					data: {
						id_siswa: $(this).attr("data-id"),
					},
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: "top-center",
						icon: "success",
						title: "Your work has been saved",
						showConfirmButton: false,
						timer: 1500,
					});

					getdata();
				});
			}
		});
	});

	$(document).on("click", "#btn-edit", function () {
		var form = new FormData();
		form.append("id_siswa", $(this).attr("data-id"));
		var settings = {
			url: rest.uri + "siswa/selectBy?id_siswa=" + $(this).attr("data-id"),
			method: "GET",
			timeout: 0,
			headers: {
				"X-key": rest.token,
				Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
			},
			processData: false,
			mimeType: "multipart/form-data",
			contentType: false,
			data: form,
		};

		$.ajax(settings).done(function (response) {
			let jsn = JSON.parse(response);
			//console.log(jsn);

			$.each(jsn.data, function (key, value) {
				$("#u_siswa_nis").val(value.siswa_nis);
				$("#u_siswa_nama").val(value.siswa_nama);
				$("#u_siswa_kelas").val(value.siswa_kelas);
				$("#u_siswa_jurusan").val(value.siswa_jurusan);
				$("#u_email").val(value.email);
				$("#id_siswa").val(value.id_siswa);
			});
		});
	});

	$("#updateform").validate({
		rules: {
			siswa_nis: {
				required: true,
				minlength: 8,
				number: true,
			},
			siswa_nama: {
				required: true,
			},
			siswa_kelas: {
				required: true,
				number: true,
			},
			siswa_jurusan: {
				required: true,
			},
			email: {
				required: true,
			},
		},
		submitHandler: function (updateform) {
			var file = $("#u_gambar")[0].files[0];
			Swal.fire({
				title: "Are you sure?",
				text: "You won't be able to revert this!",
				icon: "success",
				showCancelButton: true,
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				confirmButtonText: "Yes, Upload Produk it!",
			}).then((result) => {
				if (result.value) {
					var form = new FormData();
					//form.append("id_siswa", $('#id_siswa').val());
					form.append("siswa_nis", $("#u_siswa_nis").val());
					form.append("siswa_nama", $("#u_siswa_nama").val());
					form.append("siswa_kelas", $("#u_siswa_kelas").val());
					form.append("siswa_jurusan", $("#u_siswa_jurusan").val());
					form.append("gambar", file);
					form.append("_method", "PUT");
					form.append("email", $("#u_email").val());
					form.append("id_siswa", $("#id_siswa").val());

					var settings = {
						url: rest.uri + "siswa",
						method: "POST",
						timeout: 0,
						headers: {
							"X-key": rest.token,
							Authorization: "Basic c2lnaXRidWRpOnNpZ2l0MTIzNDU=",
						},
						processData: false,
						mimeType: "multipart/form-data",
						contentType: false,
						data: form,
					};

					$.ajax(settings).done(function (response) {
						const Toast = Swal.mixin({
							toast: true,
							position: "top-end",
							showConfirmButton: false,
							timer: 1500,
							timerProgressBar: true,
							onOpen: (toast) => {
								toast.addEventListener("mouseenter", Swal.stopTimer);
								toast.addEventListener("mouseleave", Swal.resumeTimer);
							},
						});

						Toast.fire({
							icon: "success",
							title: "Data Berhasil di Upload",
						});
						$("#updateform")[0].reset();
						$("#modelEdit").modal("hide");
						//  $(this).parent().remove();
						getdata();
					});
				}
			});
		},
	});
});
