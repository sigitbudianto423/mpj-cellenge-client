<div class="row">

    
<div class="container">
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modelId">
    Tambah Data Transaksi
    </button>
</div>

<!-- Modal -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
            <section id="form_tambah_data" class="col-md-6-sm-6 offsite-md-3">
                <div class="card">
                    <div class="card-body">
                    <form action="" id="transaksi">
                        <div class="form-grup">
                            <label for="nis">Nis</label>
                            <input class="form-control" type="text" id="nis" name="nis" placeholder="Nis">
                        </div>
                        <div class="form-grup">
                            <label for="nama_siswa">Nama Siswa</label>
                            <input class="form-control" type="text" id="nama_siswa" name="nama_siswa" placeholder="Nama Siswa" readonly>
                            <input class="form-control" type="hidden" id="id_siswa" name="id_siswa" placeholder="Nama Siswa">
                        </div>
                        <div class="form-grup">
                            <label for="kelas">kelas</label>
                            <input class="form-control" type="text" id="kelas" name="kelas" placeholder="kelas" readonly>
                        </div>
                        <div class="form-grup">
                            <label for="jurusan">Jurusan</label>
                            <input class="form-control" type="text" id="jurusan" name="jurusan" placeholder="jurusan" readonly>
                        </div>
                        <div class="form-grup">
                            <label for="nama_buku">Nama Buku</label>
                            <input class="form-control" type="text" id="nama_buku" name="nama_buku" placeholder="Nama Buku">
                        </div>
                        <div class="form-grup">
                            <label for="tgl_pinjam">Tanggal Pinjam</label>
                            <input class="form-control" type="date" id="tgl_pinjam" name="tgl_pinjam" placeholder="Nama Buku">
                        </div>
                        <div class="form-grup">
                            <label for="tgl_kembali">Tanggal kembali</label>
                            <input class="form-control" type="date" id="tgl_kembali" name="tgl_kembali" placeholder="Nama Buku">
                        </div>
                        <div> 
                            <button id="pinjam" name="pinjam" class="btn btn-success">Pinjam</button>
                        </div>
                    </form>


                    </div>            
                                        
                        </form>
                    </div>
                </div>
            </section>  

            </div>
        
        </div>
    </div>
</div>




<div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"></h1>
         
          <div class="row">
            <div class="col-lg">

<table id="table" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Nis</th>
            <th>Nama Siswa</th>
            <th>Buku</th>
            <th>tanggal Pinjam</th>
            <th>Tanggal Kembali</th>
            <th>Status</th>
            <th>Opsi</th>
            
        </tr>
    </thead>
    <tbody id="datatran">
    </tbody>
</table>

 </div>
    
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

    

      <script src="<?= base_url('asset/');?>js/transaksi.js"></script>