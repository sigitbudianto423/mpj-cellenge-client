
<div class="container">
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modelId">
    Tambah Data Siswa
    </button>
</div>

<!-- Modal -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
            <section id="form_tambah_data" class="col-md-6-sm-6 offsite-md-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" id="myform" enctype="multipart/form-data">
                                    <div class="form-group">
                                    <label for="nis">NIS</label>
                                    <input type="text"
                                        class="form-control" name="siswa_nis" id="siswa_nis" aria-describedby="helpId" placeholder="Masukan NIS">
                                    </div>                  
                                    <div class="form-group">
                                    <label for="nama">Nama Siswa</label>
                                    <input type="text"
                                        class="form-control" name="siswa_nama" id="siswa_nama" aria-describedby="helpId" placeholder="Masukan Nama">
                                        </div> 
                                    <div class="form-group">
                                    <label for="kelas">kelas</label>
                                    <input type="text"
                                        class="form-control" name="siswa_kelas" id="siswa_kelas" aria-describedby="helpId" placeholder="kelas">
                                        </div> 
                                    <div class="form-group">
                                    <label for="jurusan">jurusan</label>
                                    <input type="text"
                                        class="form-control" name="siswa_jurusan" id="siswa_jurusan" aria-describedby="helpId" placeholder="Jurusan">
                                        </div> 
                                        <div class="form-group">
                                    <label for="email">email</label>
                                    <input type="text"
                                        class="form-control" name="email" id="email" aria-describedby="helpId" placeholder="email">
                                        </div> 
                                    <div class="form-group">
                                    <label for="gambar">gambar</label>
                                    <input type="file"
                                        class="form-control" name="gambar" id="gambar" aria-describedby="helpId" required>
                                    
                                <br>
                                <center>
                                <button class="btn  btn-primary" type="submit" id="submit">Tambah Data</button>
                                </center>
                        </form>
                    </div>
                </div>
            </section>  

            </div>
        
        </div>
    </div>
</div>





<div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"></h1>
         
          <div class="row">
            <div class="col-lg">

<table id="table" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Nis</th>
            <th>Nama Siswa</th>
            <th>Kelas</th>
            <th>jurusan</th>
            <th>email</th>
            <th>gambar</th>
            <th>Opsi</th>
            
        </tr>
    </thead>
    <tbody id="data">
    </tbody>
</table>

 </div>
    
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      
     <!-- modal edit -->

<div class="modal fade" id="modelEdit" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data Siswa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
            <section id="form_edit_data" class="col-md-6-sm-6 offsite-md-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" id="updateform"> 
                                     <div class="form-group">
                                    <label for="jurusan">NIS</label>
                                    <input type="text"
                                        class="form-control" name="u_siswa_nis" id="u_siswa_nis" aria-describedby="helpId" placeholder="Jurusan">
                                                       
                                    <div class="form-group">
                                    <label for="nama">nama</label>
                                    <input type="text"
                                        class="form-control" name="u_siswa_nama" id="u_siswa_nama" aria-describedby="helpId" placeholder="nama_siswa">
                                        <input type="hidden"
                                        class="form-control"  name="id_siswa" id="id_siswa" aria-describedby="helpId" placeholder="id_siswa">
                                    
                                    <div class="form-group">
                                    <label for="kelas">kelas</label>
                                    <input type="text"
                                        class="form-control" name="u_siswa_kelas" id="u_siswa_kelas" aria-describedby="helpId" placeholder="kelas">
                                    
                                    <div class="form-group">
                                    <label for="jurusan">jurusan</label>
                                    <input type="text"
                                        class="form-control" name="u_siswa_jurusan" id="u_siswa_jurusan" aria-describedby="helpId" placeholder="Jurusan">
                                        <div class="form-group">
                                    <label for="jurusan">email</label>
                                    <input type="text"
                                        class="form-control" name="u_email" id="u_email" aria-describedby="helpId" placeholder="Jurusan">
                                    
                                    <div class="form-group">
                                    <label for="gambar">gambar</label>
                                    <input type="file"
                                        class="form-control" name="u_gambar" id="u_gambar" aria-describedby="helpId" >
                                    
                                <br>
                                <center>
                                <button class="btn  btn-primary" type="submit" id="submit">Edit Data Siswa</button>
                                </center>
                        </form>
                    </div>
                </div>
            </section>  

            </div>
        
        </div>
    </div>
</div>
<script src="<?= base_url('asset/');?>js/siswa.js"></script>

