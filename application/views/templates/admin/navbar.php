<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark orange lighten-1">
  <a class="navbar-brand" href="<?= base_url('siswa')?>">Perpusku</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
    aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-555">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('siswa')?>">Home
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('buku')?>">Buku</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('siswa')?>">Siswa</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('transaksi')?>">Transaksi</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('dikembalikan')?>">dikembalikan</a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item avatar">
      <button id="logout" class="btn btn-info">LOGOUT</button>
        <!-- <a  href="#">
         
        </a> -->
      </li>
    </ul>
  </div>
</nav>
<!--/.Navbar -->

<script>
  $("#logout").click(function (e) {
	e.preventDefault();

	Swal.fire({
					position: "top-center",
					icon: "success",
					title: "Terimah kasih",
					showConfirmButton: false,
					timer: 1500,
				});
    sessionStorage.clear();
    setTimeout(function () {
    window.location.href = rest.http + "auth/login";
  }, 1500);
});
</script>